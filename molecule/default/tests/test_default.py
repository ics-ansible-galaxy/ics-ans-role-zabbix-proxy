import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_docker(host):
    with host.sudo():
        cmd = host.command("docker ps")
    assert cmd.rc == 0


def test_zabbix_db(host):
    with host.sudo():
        cmd = host.command("docker ps --format '{{.Names}}'")
    assert "mysql" in cmd.stdout
    assert "zabbix-proxy" in cmd.stdout
